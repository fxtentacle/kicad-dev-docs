---
title: macOS
weight: 12
summary: Instructions for macOS using cmake and clang
tags: ["macos"]
---

:toc:

== Building KiCad on macOS

As of V5, building and packaging for macOS can be done using https://gitlab.com/kicad/packaging/kicad-mac-builder[kicad-mac-builder],
which downloads, patches, builds, and packages for macOS.  It is used to create the official
releases and nightlies, and it reduces the complexity of setting up a build environment to a command
or two.  Usage of kicad-mac-builder is detailed at on its website.

If you wish to build without kicad-mac-builder, please use the following and its source code
as reference. Building on macOS requires building dependency libraries that require patching
in order to work correctly.

In the following set of commands, replace the macOS version number (i.e. 10.11) with the desired
minimum version.  It may be easiest to build for the same version you are running.

KiCad currently won't work with a stock version of wxWidgets that can be downloaded or
installed by package managers like MacPorts or Homebrew. To avoid having to deal with
patches a https://github.com/KiCad/wxWidgets[KiCad fork of wxWidgets] is being maintained on GitHub. All the needed patches
and some other fixes/improvements are contained in the `kicad/macos-wx-3.0` branch.

To perform a wxWidgets build, execute the following commands:

[source,sh]
```
cd <your wxWidgets build folder>
git clone -b kicad/macos-wx-3.0 https://gitlab.com/kicad/code/wxWidgets.git
mkdir wx-build
cd wx-build
../wxWidgets/configure \
    --prefix=`pwd`/../wx-bin \
    --with-opengl \
    --enable-aui \
    --enable-html \
    --enable-stl \
    --enable-richtext \
    --with-libjpeg=builtin \
    --with-libpng=builtin \
    --with-regex=builtin \
    --with-libtiff=builtin \
    --with-zlib=builtin \
    --with-expat=builtin \
    --without-liblzma \
    --with-macosx-version-min=10.11 \
    --enable-universal-binary=i386,x86_64 \
    CC=clang \
    CXX=clang++
make
make install
```

If everything works you will find the wxWidgets binaries in `<your wxWidgets build folder>/wx-bin`.
Now, build a basic KiCad without Python scripting using the following commands:


[source,sh]
```
cd <your kicad source mirror>
mkdir -p build/release
mkdir build/debug               # Optional for debug build.
cd build/release
cmake -DCMAKE_C_COMPILER=clang \
        -DCMAKE_CXX_COMPILER=clang++ \
        -DCMAKE_OSX_DEPLOYMENT_TARGET=10.11 \
        -DwxWidgets_CONFIG_EXECUTABLE=<your wxWidgets build folder>/wx-bin/bin/wx-config \
        -DKICAD_SCRIPTING=OFF \
        -DKICAD_SCRIPTING_MODULES=OFF \
        -DKICAD_SCRIPTING_WXPYTHON=OFF \
        -DCMAKE_INSTALL_PREFIX=../bin \
        -DCMAKE_BUILD_TYPE=Release \
        ../../
make
make install
```

If the CMake configuration fails, determine the missing dependencies and install them on your
system or disable the corresponding KiCad feature. If everything works you will get self-contained
application bundles in the `build/bin` folder.

Building KiCad with Python scripting is more complex and won't be covered in detail here.
You will have to build wxPython against the wxWidgets source of the KiCad fork - a stock wxWidgets
that might be bundled with the wxPython package won't work. Please see wxPython documentation
or [macOS bundle build scripts][] (`compile_wx.sh`) on how to do this. Then, use a CMake
configuration as follows to point it to your own wxWidgets/wxPython:

[source,sh]
```
cmake -DCMAKE_C_COMPILER=clang \
        -DCMAKE_CXX_COMPILER=clang++ \
        -DCMAKE_OSX_DEPLOYMENT_TARGET=10.9 \
        -DwxWidgets_CONFIG_EXECUTABLE=<your wxWidgets build folder>/wx-bin/bin/wx-config \
        -DPYTHON_EXECUTABLE=<path-to-python-exe>/python \
        -DPYTHON_SITE_PACKAGE_PATH=<your wxWidgets build folder>/wx-bin/lib/python2.7/site-packages \
        -DCMAKE_INSTALL_PREFIX=../bin \
        -DCMAKE_BUILD_TYPE=Release \
        ../../
```
